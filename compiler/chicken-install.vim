" Vim compiler file
" Compiler:        CHICKEN Scheme Compiler (chicken-install)
" Author:          Evan Hanson <evhan@foldling.org>
" Maintainer:      Evan Hanson <evhan@foldling.org>
" Latest Revision: 2024 Jun 23

if exists('current_compiler')
  finish
endif

runtime compiler/chicken.vim

let current_compiler = 'chicken-install'

CompilerSet makeprg=chicken-install\ -no-install
