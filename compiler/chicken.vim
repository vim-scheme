" Vim compiler file
" Compiler:        CHICKEN Scheme Compiler
" Author:          Jeremy Steward <jeremy@thatgeoguy.ca>
"                  Evan Hanson <evhan@foldling.org>
" Maintainer:      Evan Hanson <evhan@foldling.org>
" Latest Revision: 2024 Jun 23

if exists('current_compiler')
  finish
endif

let current_compiler = 'chicken'

let s:save_cpo = &cpo
set cpo&vim

if exists(':CompilerSet') != 2
  command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet makeprg=make

CompilerSet errorformat=%t%.%#:\ (%f:%l)\ -\ %m
CompilerSet errorformat+=(\"%f:%l\"\ %t%\\a%\\+\ %m)
CompilerSet errorformat+=%E%>Syntax\ error:%\\?\ (%f:%l):%\\?\ %m,%-C,%Z%\\s%\\s%#%m
CompilerSet errorformat+=%W%>Warning:\ %m,%C%>\ \ In\ file\ `%f:%l'%.%#,%C%>\ \ %m,%-C%>,%Z\ \ \ \ %m
CompilerSet errorformat+=%N%>Note:\ %m,%C%>\ \ In\ file\ `%f:%l'%.%#,%C%>\ \ %m,%-C%>,%Z\ \ \ \ %m
CompilerSet errorformat+=%-GError:\ shell\ invocation\ failed%.%#
CompilerSet errorformat+=%-GError:\ shell\ command\ terminated%.%#
CompilerSet errorformat+=%-G%.%#
CompilerSet errorformat+=%-G%.%#,. " https://github.com/tpope/vim-dispatch/issues/76

let &cpo = s:save_cpo
unlet s:save_cpo
