vim-scheme
==========

Scheme support for Vim (R7RS and CHICKEN).

Installation
------------

This project provides the official Scheme runtime files since Vim 8.0,
so you may not need to do anything to use it.

If you have an older version of Vim, or you would like make sure you're
always using the newest version of the runtime files, you can install
<https://git.sr.ht/~evhan/vim-scheme> using your plugin manager of
choice.

Usage
-----

To enable CHICKEN-specific features, add the following to your startup
file:

    let g:is_chicken = 1

To enable Scheme syntax for sld (library definition) files, add:

    au BufNewFile,BufRead *.sld setl filetype=scheme

Do the same for any other extensions you'd like to associate. Commonly,
these include `ss`, `sch`, `rkt`, and (in the case of CHICKEN) `egg`.

Compiler
--------

When CHICKEN is selected (see above), this plugin will set `makeprg`
according to the following rules:

1. If an egg file is present: `chicken-install -n`
2. If a Makefile is present: `make`
3. Otherwise: `csc`

Acknowledgements
----------------

Thanks to `TheLemonMan` and `DeeEff` in `#chicken` for lots of helpful
feedback. Thanks also to Andrey Mishchenko, Chuan Wei Foo, Dorai Sitaram
and Jeremy Stewart for reporting various problems and suggesting features.

Links
-----

This repository is hosted at <https://git.foldling.org/vim-scheme.git>
and mirrored to <https://git.sr.ht/> for availability and issue
tracking.

 * Sources: <https://git.sr.ht/~evhan/vim-scheme>
 * Issues: <https://todo.sr.ht/~evhan/vim-scheme>

The individual runtime files can also be downloaded directly from
<https://foldling.org/vim/>.

License
-------

Public Domain.
